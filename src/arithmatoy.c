#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#include "utils.h"

int VERBOSE = 0;

const char *get_all_digits() { return "0123456789abcdefghijklmnopqrstuvwxyz"; }
const size_t ALL_DIGIT_COUNT = 36;

void arithmatoy_free(char *number) { free(number); }

char *arithmatoy_add(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "add: entering function\n");
  }

  // Fill the function, the goal is to compute lhs + rhs
  // You should allocate a new char* large enough to store the result as a
  // string Implement the algorithm Return the result

  lhs = drop_leading_zeros(lhs);
  rhs = drop_leading_zeros(rhs);
  size_t len_lhs = strlen(lhs), len_rhs = strlen(rhs), sum;//, digit;
  size_t max_len = (len_lhs > len_rhs) ? len_lhs : len_rhs;
  unsigned int rest = 0;
  size_t result_index = max_len + 1;
  char *result = calloc(max_len + 2, sizeof(char)); // Allouer de la mémoire pour stocker le résultat, 1 pour nul caractère et 1 pour la retenue (rest)
  //result[result_index] = '\0';
  // Parcourir les chaînes de caractères `lhs` et `rhs` de droite à gauche, effectuer l'addition bit à bit et stocker le résultat dans la chaîne `result`
  while (len_lhs > 0 || len_rhs > 0) {
    size_t lhs_digit = (len_lhs > 0 ? get_digit_value(lhs[--len_lhs]) : 0);
    size_t rhs_digit = (len_rhs > 0 ? get_digit_value(rhs[--len_rhs]) : 0);
    if (VERBOSE) {
      fprintf(stderr, "add: digit %c digit %c carry %u\n", to_digit(lhs_digit), to_digit(rhs_digit), rest);
    }
    sum = lhs_digit + rhs_digit + rest;
    rest = sum / base;
    sum = sum % base;
    if (VERBOSE) {
      fprintf(stderr, "add: result: digit %c carry %u\n", to_digit(sum), rest);
    }
    result[--result_index] = to_digit(sum);
  }

  if (rest != 0) {
    if (VERBOSE) {
      fprintf(stderr, "add: final carry %u\n", rest);
    }
    char *result2 = calloc(max_len + 3 , sizeof(char));
    result2[0] = to_digit(rest);
    //result[--result_index] = to_digit(rest);
    strcpy(result2 + 1, result + result_index);
    arithmatoy_free(result);
    return result2;
  }
return result + result_index;
}

char *arithmatoy_sub(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "sub: entering function\n");
  }

  // Fill the function, the goal is to compute lhs - rhs (assuming lhs > rhs)
  // You should allocate a new char* large enough to store the result as a
  // string Implement the algorithm Return the result


  lhs = drop_leading_zeros(lhs);
  rhs = drop_leading_zeros(rhs);
  if (strcmp(lhs, rhs) == 0) {
    char *result = malloc(2);
    result[0] = '0';
    result[1] = '\0';
    return result;
  } else if (strcmp(rhs, "0") == 0) {
    return strdup(lhs);
  }
  size_t len_lhs = strlen(lhs), len_rhs = strlen(rhs);
  size_t max_len = (len_lhs > len_rhs) ? len_lhs : len_rhs;
  int rest = 0;
  int result_index = max_len - 1;
  char *result = malloc(max_len + 2); // Allouer de la mémoire pour stocker le résultat, 1 pour nul caractère et 1 pour la retenue (rest)
  result[result_index + 1] = '\0';
  if (len_lhs < len_rhs || (len_lhs == len_rhs && strcmp(lhs, rhs) < 0)) {
    free(result);
    return NULL;
  }

    while (len_lhs > 0 || len_rhs > 0) {
        size_t lhs_digit = (len_lhs > 0 ? get_digit_value(lhs[--len_lhs]) : 0);
        size_t rhs_digit = (len_rhs > 0 ? get_digit_value(rhs[--len_rhs]) : 0);
        if (VERBOSE) {
          fprintf(stderr, "sub: digit %c digit %c carry %u\n", to_digit(lhs_digit), to_digit(rhs_digit), rest);
        }
        int difference = lhs_digit - rhs_digit - rest + base;
        rest = (difference >= base ? 0 : 1);
        size_t digit = difference % base;
        if (VERBOSE) {
          fprintf(stderr, "sub: result: digit %c carry %u\n", to_digit(digit), rest);
        }
        result[result_index--] = to_digit(digit);
    }

    // Traitement de la retenue éventuelle restante
    while (result[result_index + 1] == '0') {
        ++result_index;
    }

    if (result_index == max_len - 1) { // le résultat est nul
        //result[0] = '0';
        //result[1] = '\0';
        return NULL;
    } else { // le résultat est non nul
        return result + result_index + 1;
    }
}

char *arithmatoy_mul(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "mul: entering function\n");
  }

  lhs = drop_leading_zeros(lhs);
  rhs = drop_leading_zeros(rhs);

  // 0 x 0 =  "0"
  if (strcmp(lhs, "0") == 0 || strcmp(rhs, "0") == 0) {
    return "0";
  }

  size_t len_lhs = strlen(lhs), len_rhs = strlen(rhs);
  size_t max_len = len_lhs + len_rhs;
  unsigned int *result = calloc(max_len, sizeof(unsigned int));

  // doubl loop pour la multiplication des chiffrs un par un 
  for (size_t i = 0; i < len_lhs; i++) {
    unsigned int carry = 0;
    unsigned int digit_lhs = get_digit_value(lhs[len_lhs - 1 - i]);
    for (size_t j = 0; j < len_rhs; j++) {
      unsigned int digit_rhs = get_digit_value(rhs[len_rhs - 1 - j]);
      unsigned int sum = result[i + j] + digit_lhs * digit_rhs + carry;
      result[i + j] = sum % base;
      carry = sum / base;
    }
    if (carry > 0) {
      result[i + len_rhs] += carry;
    }
  }

 // Supprimer les zéros en trop du résultat
  size_t result_len = max_len;
  while (result_len > 1 && result[result_len - 1] == 0) {
    result_len--;
  }

  // convertir unsigned int en char 
  char *result_str = calloc(result_len + 1, sizeof(char));
  for (size_t i = 0; i < result_len; i++) {
    result_str[result_len - 1 - i] = to_digit(result[i]);
  }
  result_str[result_len] = '\0';

  return result_str;
}

// Here are some utility functions that might be helpful to implement add, sub
// and mul:

unsigned int get_digit_value(char digit) {
  // Convert a digit from get_all_digits() to its integer value
  if (digit >= '0' && digit <= '9') {
    return digit - '0';
  }
  if (digit >= 'a' && digit <= 'z') {
    return 10 + (digit - 'a');
  }
  return -1;
}

char to_digit(unsigned int value) {
  // Convert an integer value to a digit from get_all_digits()
  if (value >= ALL_DIGIT_COUNT) {
    debug_abort("Invalid value for to_digit()");
    return 0;
  }
  return get_all_digits()[value];
}

char *reverse(char *str) {
  // Reverse a string in place, return the pointer for convenience
  // Might be helpful if you fill your char* buffer from left to right
  const size_t length = strlen(str);
  const size_t bound = length / 2;
  for (size_t i = 0; i < bound; ++i) {
    char tmp = str[i];
    const size_t mirror = length - i - 1;
    str[i] = str[mirror];
    str[mirror] = tmp;
  }
  return str;
}

const char *drop_leading_zeros(const char *number) {
  // If the number has leading zeros, return a pointer past these zeros
  // Might be helpful to avoid computing a result with leading zeros
  if (*number == '\0') {
    return number;
  }
  while (*number == '0') {
    ++number;
  }
  if (*number == '\0') {
    --number;
  }
  return number;
}

void debug_abort(const char *debug_msg) {
  // Print a message and exit
  fprintf(stderr, debug_msg);
  exit(EXIT_FAILURE);
}
