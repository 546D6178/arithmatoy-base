# Aucun n'import ne doit être fait dans ce fichier


def nombre_entier(n: int) -> str:

    if n == 0 :
        return '0'
    temp = '' 
    for i in range (0, n):
        temp += 'S'
    temp += '0'
    return(temp)


def S(n: str) -> str:
    n = 'S' + n
    return n


def addition(a: str, b: str) -> str:
    if a == '0':
        return b

    if a[0] == 'S' :
        return S(addition(a[1:], b))
    

def multiplication(a: str, b: str) -> str: #Sn * x = x +(n*x)
    if a == '0':
        return '0'
    if a[0] == 'S':
        return addition(multiplication(a[1:], b), b) 
    

#n! = n * (n-1)!
def facto_ite(n: int) -> int:
    if n == 0:
        return 1
    res = 1
    for i in range (2, n+1): 
        res = res * i
    return res
    

def facto_rec(n: int) -> int:
    if n == 0:
        return 1
    return (n * facto_rec(n-1))

def fibo_rec(n: int) -> int:
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return ((fibo_rec(n-1)) + (fibo_rec(n-2)))


def fibo_ite(n: int) -> int:
    if n == 0:
        return 0
    elif n == 1:
        return 1
    elif n == 2:
        return 1
    res = 0
    fib1 = 1
    fib2 = 1
    for i in range (3, n+1):
        res = fib1 + fib2
        fib2 = fib1
        fib1 = res 
    return res


def golden_phi(n: int) -> int:
    return fibo_ite(n+1) / fibo_ite(n)


def sqrt5(n: int) -> int:
    return 2 * golden_phi(n) -1


def pow(a: float, n: int) -> float: ##Exponentiation_by_squaring
    if n == 0:
        return 1
    elif n == 1:
        return a
    elif n < 0:
        return 1 / pow(a, -n)
    elif n % 2 == 0:
        return pow(a * a, n // 2)
    else:
        return a * pow(a, n - 1)